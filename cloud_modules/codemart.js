var AV = require('leanengine');
var request = require('request');
var nodemailer = require('nodemailer');

var Cookie = 'exp=89cd78c2; withGlobalSupport=true; _ga=GA1.2.1082465009.1528175972; user-vip=false; disable-quote-new=true; _gid=GA1.2.2104388867.1529906607; mid=2eef218d-03b4-467c-a0e8-68388148f221; JSESSIONID=itb52zexdawq1lioidivayr8t';
var message = `（不一定方便接电话，烦请加QQ，十分感谢）
全栈工程师，8年开发经验，产品、设计、开发全能；
精通JavaScript，也熟悉其他主流语言；
精通所有主流js框架和库，包括但不限于react,vue,node.js,express,react-native,electron等等；
精通PC/手机网站前/后端开发、微信/小程序开发、ReactNative手机客户端开发、Electron桌面客户端开发。`;

const transporter = nodemailer.createTransport({
    host: 'smtp.exmail.qq.com',
    port: 465,
    secure: true,
    auth: {
        user: 'anho@xingzi.ink',
        pass: 'ZZW199121o',
    }
});

function requestPromise(options) {
  return new Promise((resolve, reject) => {
    request(options, (err, res, body) => {
      if (err) reject(err);
      else resolve(body);
    })
  })
}

AV.Cloud.define('codemart', function(req) {
  const options = {
    uri: 'https://codemart.com/api/project?page=1',
    headers: { Accept: "application/json" },
  }
  const query = new AV.Query('Record');
  query.equalTo('key', 'lastCodemartId');
  return Promise.all([
    requestPromise(options),
    query.first(),
  ]).then(([res, record]) => {
    const lastCodemartId = parseInt(record.get('value'));
    let projects;
    try {
      projects = JSON.parse(res).rewards;
    } catch (error) {
      console.error(res);
    }
    if (projects[0].id <= lastCodemartId) return;
    record.set('value', projects[0].id.toString());
    record.save();
    const newProjects = [];
    projects.forEach(project => {
      if (project.id <= lastCodemartId) return;
      if (project.roles == '开发团队') return;
      newProjects.push(Object.assign({}, project));
    })
    if (newProjects.length === 0) return;
    return Promise.all(newProjects.map(project => {
      const options = {
        uri: 'https://codemart.com/api/join',
        headers: { Cookie },
        form: `message=${message}&reward_id=${project.id}&secret=1&role_type=-1&projectIdArr%5B%5D=102370&projectIdArr%5B%5D=102371&projectIdArr%5B%5D=102372&projectIdArr%5B%5D=102373&projectIdArr%5B%5D=102374`,
        method: 'POST',
      }
      return requestPromise(options);
    }))
  }).then(res => {
    if (!res) return;
    res.forEach(data => {
      const json = JSON.parse(data);
      if (json.code === 0) {
        transporter.sendMail({
            from: '"CodeMart" <anho@xingzi.ink>', // sender address
            to: 'anho@xingzi.ink', // list of receivers
            subject: `${json.data.title}`, // Subject line
            html: `<pre>${JSON.stringify(json.data, null, 2)}</pre>`, // html body
        });
      }
    })
    setTimeout(() => {
      const options = {
        uri: 'https://codemart.com/api/project/mine?applyStatus=CHECKING&page=1&size=10',
        headers: {
          Cookie,
          Accept: 'application/json',
        },
      }
      requestPromise(options).then(res => {
        const data = JSON.parse(res);
        if (data.rewards.length === 10) {
          transporter.sendMail({
              from: '"CodeMart" <anho@xingzi.ink>', // sender address
              to: 'anho@xingzi.ink', // list of receivers
              subject: `报名项目达到10个`, // Subject line
              html: ``, // html body
          });
        }
      })
    }, 1000 * 30);
    return
  })
})