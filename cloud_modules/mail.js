var AV = require('leanengine');
const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
  host: 'smtp.exmail.qq.com',
  port: 465,
  secure: true,
  auth: {
    user: 'fashion.footwear@lipple.co',
    pass: 'ZZW199121o',
  },
})
const transporter2 = nodemailer.createTransport({
  host: 'smtp.exmail.qq.com',
  port: 465,
  secure: true,
  auth: {
    user: 'fashion.footwear@xingzi.ink',
    pass: 'ZZW199121o',
  },
})

/**
 * 一个简单的云代码方法
 */
AV.Cloud.define('callUp', function(request) {
  return 'ok';
});

function sendMail(template, address) {
  return new Promise(resolve => {
    // if (Math.random() > 0.5) {
      transporter.sendMail({
        from: '"穿潮鞋" <fashion.footwear@lipple.co>',
        to: address.get('address'),
        subject: template.get('title'),
        html: template.get('htmlContent'),
      }, (err, info) => {
        if (err) {
          console.error(err);
          resolve(err);
        } else {
          console.log(info);
          address.set('sended', template);
          address.save().then(resolve, resolve);
        }
      })
    // } else {
    //   transporter2.sendMail({
    //     from: '"穿潮鞋" <fashion.footwear@xingzi.ink>',
    //     to: address.get('address'),
    //     subject: template.get('title'),
    //     html: template.get('htmlContent'),
    //   }, (err, info) => {
    //     if (err) {
    //       console.error(err);
    //       resolve(err);
    //     } else {
    //       console.log(info);
    //       address.set('sended', template);
    //       address.save().then(resolve, resolve);
    //     }
    //   })
    // }
  })
}

function batchSendMail(template, addressList, index = 0) {
  if (index >= addressList.length) return;
  return sendMail(template, addressList[index]).then(() => batchSendMail(template, addressList, index + 1));
}

AV.Cloud.define('sendMail', function(request) {
  const templateQuery = new AV.Query('MailTemplate');
  templateQuery
  .equalTo('current', true)
  .descending('createdAt')
  .first()
  .then(template => {
    const addressQuery = new AV.Query('MailAddress');
    return Promise.all([
      template, 
      addressQuery
      .notEqualTo('sended', template)
      .descending('createdAt')
      .limit(100)
      .find(),
    ]);
  }).then(([template, addressList]) => {
    return batchSendMail(template, addressList);
  }).then(() => {
    console.log('Queue complete!')
  })
})

AV.Cloud.define('testMail', function(request) {
  const templateQuery = new AV.Query('MailTemplate');
  templateQuery
  .equalTo('current', true)
  .descending('createdAt')
  .first()
  .then(template => {
    const testAddress = new AV.Object('MailAddress');
    testAddress.set('address', '414223432@qq.com');
    return sendMail(template, testAddress);
  });
})
