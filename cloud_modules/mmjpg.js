var AV = require('leanengine');
var request = require('request');
var cheerio = require('cheerio');

AV.Cloud.define('mmjpg', () => {
  return new Promise(resolve => {
    const data = [];

    function crawler(id, number) {
      console.log(`fetching mmjpg ${id} : ${number}`)
      request(`http://mmjpg.com/mm/${id}/${number}`, (err, res, body) => {
        const $ = cheerio.load(body);
        if ($('title').text().startsWith('页面不存在')) {
          AV.Object.saveAll(data).then(resolve);
          return;
        }
        const $a = $('#page a');
        const max = parseInt($a.eq($a.length - 2).attr('href').split('/').slice(-1)[0]) 
        let link = $('#content img').data('img');
        const mmjpg = new AV.Object('MMJPG');
        mmjpg.set('link', link);
        data.push(mmjpg);
        if (number >= max) {
          crawler(id + 1, 1);
        } else {
          crawler(id, number + 1);
        }
      })
    }

    const query = new AV.Query('MMJPG');
    query.descending('num');
    query.limit(1);
    query.find().then(mm => {
      const splits = mm[0].get('link').split('/');
      const lastId = parseInt(splits[splits.length - 2]);
      crawler(lastId, 1);
    });
  })
})